﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;

namespace ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new HttpClient();

            var disc = await client.GetDiscoveryDocumentAsync("https://localhost:5001");

            if (disc.IsError) {
                Console.WriteLine(disc.Error);
                return;
            }

            var tokenResponse = await client.RequestClientCredentialsTokenAsync( new ClientCredentialsTokenRequest
            {
                 Address = disc.TokenEndpoint,
                 ClientId = "m2m.client",
                 ClientSecret = "homework",
                 Scope = "scope1"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }


            client.SetBearerToken(tokenResponse.AccessToken);
            var response = await client.GetAsync("https://localhost:6001/api");
            if (response.IsSuccessStatusCode)
            {
                var body = await response.Content.ReadAsStringAsync();
                Console.WriteLine(body);
            }
            else
            {
                Console.WriteLine(tokenResponse.Json);
                Console.WriteLine(response.StatusCode);
            }
        }
    }
}
